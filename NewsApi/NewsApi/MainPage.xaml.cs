﻿
using Windows.UI.Xaml.Controls;


using System.Collections.ObjectModel;
using NewsApi.Models;

namespace NewsApi
{
    public sealed partial class MainPage : Page
    {
        ObservableCollection<Article> Articles;
        public MainPage()
        {
            this.InitializeComponent();
            Articles = new ObservableCollection<Article>();
            GetAR("bitcoin", "2019-10-10", "publishedAt", "582b6408924c44e4be66bd79a318ddd0");
        }

        private async void GetAR(string q, string from, string SortBy, string ApiKey)
        {
            var url = string.Format("https://newsapi.org/v2/everything?q=fashion&from=2019-10-10&sortBy=publishedAt&apiKey=582b6408924c44e4be66bd79a318ddd0", q, from, SortBy, ApiKey);
            var news = await New.GetNew(url) as RootObject;
            news.articles.ForEach(n => {
                Articles.Add(n);
            });
        }

        private void View_ItemClick(object sender, ItemClickEventArgs e)
        {
            Article ar = e.ClickedItem as Article;
            Frame.Navigate(typeof(ArticleView), ar);
        }
    }
}
