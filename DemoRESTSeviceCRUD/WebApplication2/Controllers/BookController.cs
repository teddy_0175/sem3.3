﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class BookController : Controller
    {
        // GET: Book
        public ActionResult Index()
        {
            BookServiceClient bsc = new BookServiceClient();
            ViewBag.ListBooks = bsc.getAllBook();
            return View();

        }
    }
}