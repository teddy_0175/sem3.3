﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class BookController : Controller
    {
        // GET: Book
        public ActionResult Index()
        {
            BookServiceClient bsc = new BookServiceClient();
            ViewBag.ListBooks = bsc.GetAllBook();
            return View();

        }
    }
}