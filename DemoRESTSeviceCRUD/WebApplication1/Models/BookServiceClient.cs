﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace WebApplication1.Models
{
    public class BookServiceClient
    {
        string BASE_URL = "http://localhost:53754/BookSevices.svc";

        public List<Book> GetAllBook()
        {
             var syncClient = new WebClient();
            var content = syncClient.DownloadString(BASE_URL + "Books");
            var json_serializer = new JavaScriptSerializer();
            return json_serializer.Deserialize<List<Book>>(content);

        }
    }
}