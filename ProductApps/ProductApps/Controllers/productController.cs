﻿using ProductApps.context;
using ProductApps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc; 

namespace ProductApps.Controllers
{
    
    public class productController : Controller
    {
        private ProductContext db = new ProductContext();
        // GET: product
        public ActionResult Index()
        {
            return View(db.products.ToList());
        }

        // GET: product/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            product product = db.products.Find(id);
            if (product == null)
                return HttpNotFound();
            return View(product);
            return View();
        }

        // GET: product/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: product/Create
        [HttpPost]
        public ActionResult Create(product product)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    db.products.Add(product);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(product);
                // TODO: Add insert logic here

    
            }
            catch
            {
                return View();
            }
        }

        // GET: product/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            product product = db.products.Find(id);
            if (product == null)
                return HttpNotFound(); 
            return View(product);
        }

        // POST: product/Edit/5
        [HttpPost]
        public ActionResult Edit(product product)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    db.Entry(product).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: product/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            product product = db.products.Find(id);
            if (product == null)
                return HttpNotFound();
            return View(product);
        }

        // POST: product/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id, product prod)
        {
            try
            {
                product product = new product();
                if(ModelState.IsValid)
                {
                    if (id == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    product = db.products.Find(id);
                    if (product == null)
                        return HttpNotFound();
                    db.products.Remove(product);
                    db.SaveChanges();
                    return RedirectToAction("Index");

                }
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
