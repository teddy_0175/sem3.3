﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ProductApps.Models;

namespace ProductApps.context
{
    public class ProductContext : DbContext
    {
        public DbSet<product> products { get; set; }
    }
}