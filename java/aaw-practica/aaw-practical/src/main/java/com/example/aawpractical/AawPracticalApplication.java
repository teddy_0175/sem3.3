package com.example.aawpractical;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AawPracticalApplication {

    public static void main(String[] args) {
        SpringApplication.run(AawPracticalApplication.class, args);
    }

}
