package com.example.reponsitory;

import com.example.model.product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookDao {
    public static Boolean insertPhone(product product) throws SQLException {
        String sql = "INSERT INTO phone (name, brand, price, description) VALUES (?, ?, ?, ?)";

        Connection connection = DBconnection.getConnection();
        PreparedStatement statement = connection.prepareStatement(sql);

        statement.setString(1, product.getName());
        statement.setString(2, product.getBrand());
        statement.setFloat(3, product.getPrice());
        statement.setString(4, product.getDescription());

        return statement.executeUpdate() > 0;
    }

    public static List<product> selectAllPhone() throws SQLException {
        List<product> listPhone = new ArrayList<product>();

        String sql = "SELECT * FROM store";

        Connection connection = DBconnection.getConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {
            String name = resultSet.getString("name");
            String brand = resultSet.getString("brand");
            int price = resultSet.getInt("price");
            String description = resultSet.getString("description");

            product product = new product(name, brand, price, description);
            listPhone.add(product);
        }

        return listPhone;
    }

}
