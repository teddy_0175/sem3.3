package com.example.controller;

import com.example.model.product;
import com.example.reponsitory.BookDao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class PhoneServlet extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        String name = request.getParameter("name");
        String brand = request.getParameter("brand");
        String price = request.getParameter("price ");
        String description = request.getParameter("description");

        try {
            product product = new product(name, brand, price, description);
            BookDao.insertPhone(product);
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }

        response.sendRedirect("/WCDPractical_war_exploded");
    }

}

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        List<product> phoneList = null;
        try {
            phoneList = BookDao.(product);
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        request.setAttribute("phoneList", phoneList);
        request.getRequestDispatcher("listphone.jsp").include(request, response);

    }
}
