﻿using System.Web;
using System.Web.Mvc;

namespace LINQwhithasp.NETMVC5
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
