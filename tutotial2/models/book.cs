﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tutotial2.models
{
    public class book
    {
        public int BookID { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string CoverImage { get; set; }
    }
    public class BookManager
    {
        public static List<book> GetBooks()
        {
            var books = new List<book>();

            books.Add(new book { BookID = 1, Title = "Vulpate", Author = "Futurun", CoverImage = "Assets/Books/1.png" });
            books.Add(new book { BookID = 2, Title = "Marim", Author = "Sequiter Que", CoverImage = "Assets/Books/2.png" });
            books.Add(new book { BookID = 3, Title = "Elit", Author = "Tempor", CoverImage = "Assets/Books/3.png" });
            books.Add(new book { BookID = 4, Title = "Etiam", Author = "Option", CoverImage = "Assets/Books/4.png" });
            books.Add(new book { BookID = 5, Title = "Feguait", Author = "Accumsan", CoverImage = "Assets/Books/5.png" });
            books.Add(new book { BookID = 6, Title = "Nonummy", Author = "Lengunt Xaepius", CoverImage = "Assets/Books/6.png" });
            books.Add(new book { BookID = 7, Title = "Nostrud", Author = "Eleifend", CoverImage = "Assets/Books/7.png" });
            books.Add(new book { BookID = 8, Title = "per Modo", Author = "Vero tation", CoverImage = "Assets/Books/8.png" });
            books.Add(new book { BookID = 9, Title = "Suscipit Ad", Author = "Jack Tipbles", CoverImage = "Assets/Books/9.png" });
            books.Add(new book { BookID = 10, Title = "Decima", Author = "Tuffy Tipbles", CoverImage = "Assets/Books/10.png" });
            books.Add(new book { BookID = 11, Title = "Erat", Author = "Volupat", CoverImage = "Assets/Books/11.png" });
            books.Add(new book { BookID = 12, Title = "Consequat", Author = "Est Possim", CoverImage = "Assets/Books/12.png" });
            books.Add(new book { BookID = 13, Title = "Aliquip", Author = "Magna", CoverImage = "Assets/Books/13.png" });

            return books;
        }
    }
}
